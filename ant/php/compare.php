<?php

$prefix = PHP_EOL . PHP_EOL . '<------------------------------' . PHP_EOL;
$suffix = PHP_EOL . '<------------------------------' . PHP_EOL . PHP_EOL;

if (!isset($_SERVER['argv'])) {
    throw new Exception($prefix . 'Can\'t compare tables. "register_argc_argv" parameter on php.ini must be "TRUE".' . $suffix);
}

if ($_SERVER['argc'] <= 1) {
    throw new Exception($prefix . 'Script can\'t work without parameters.' . $suffix);
}

$script_tables = array(
                     'settings' => explode(' ', $_SERVER['argv'][1]),
                     'data'     => explode(' ', $_SERVER['argv'][2]),
);

$dev_tables = explode(PHP_EOL, $_SERVER['argv'][3]);
$prod_tables = explode(PHP_EOL, $_SERVER['argv'][4]);

$not_registred_tables = array();
foreach ($script_tables as $type => $tables) {
    foreach ($tables as $table) {

        $dev_key = array_search($table, $dev_tables);
        if ($dev_key !== FALSE) {
            unset($dev_tables[$dev_key]);
        } else {
            $not_registred_tables['dev_extra'][$type][] = $table;
        }

        $prod_key = array_search($table, $prod_tables);
        if ($prod_key !== FALSE) {
            unset($prod_tables[$prod_key]);
        // Mark as "Lost" only "data" type tables because data tables data gives from Production Server
        } elseif ('data' == $type) {
            $not_registred_tables['prod_lost'][$type][] = $table;
        }

    }
}


if (count($prod_tables)) {
    $not_registred_tables['prod_extra'] = $prod_tables;
}
if (count($dev_tables)) {
    $not_registred_tables['dev_lost'] = $dev_tables;
}

if (!empty($not_registred_tables['dev_extra'])) {
    $message = 'This tables must be removed from script:';
    foreach ($not_registred_tables['dev_extra'] as $type => $tables) {
        $message .= PHP_EOL . strtoupper($type) . ' SECTION: ' . implode(', ', $tables);
    }
    throw new Exception($prefix . $message . $suffix);
}
if (!empty($not_registred_tables['dev_lost'])) {
    throw new Exception($prefix . 'This tables must be added to script tables list to "Settings" or "Data" sectionn:' . PHP_EOL . 
                        print_r($not_registred_tables['dev_lost'], TRUE) . PHP_EOL .
                        'WARNING: if You add table to "Data" section You must move this table to production DB at first.' . $suffix);
}
if (!empty($not_registred_tables['prod_extra'])) {
    echo PHP_EOL . 'Tables:' . PHP_EOL . print_r($not_registred_tables['prod_extra'], TRUE) . PHP_EOL .
         'Removed from Development database but presented on Production DB.' . PHP_EOL . PHP_EOL;
}
if (!empty($not_registred_tables['prod_lost'])) {
    throw new Exception($prefix . 'Check manualy production tables structure. This tables lost on production:' . PHP_EOL . 
                        print_r($not_registred_tables['prod_lost'], TRUE) . $suffix);
}
