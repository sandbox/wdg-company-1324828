/* $Id$ */

 ---------- SUMMARY ----------
Synchro module allow make deploy database changes to Production server on automatic way through Apache Ant.

Deployment process splits to 3 steps:
 1. Move all data changes of Production site DB to Development site DB and put Production to maintenance mode.
 2. Final testing of Development site with actual data.
 3. Deploy tested database to production.

 ---------- INSTALL ----------
Copy module folder to sites/all/modules or sites/[your_site]/modules folder
of Your Drupal site and enable it on modules list page.


 ---------- USAGE ----------
1. Assemble build.xml file for Apache Ant:

 - Go to "Configuration" -> "Synchro settings" page.
   - On first visit of "Synchro settings" page module make Scanning variables.
   - Only after finish scanning process "Synchro settings" page will be available.
   - In future if You need scan variables again (if You install/remove some modules). You can use one of this 2 ways:
     - Go to "Configuration" -> "Synchro settings" -> "Synchro scan variables sets" menu item.
     - On "Configuration" -> "Synchro settings" page click by tab "Synchro scan variables sets" on top part of page.

 - On "Synchro settings" page You need setup this points:
   - URL of Development and Production sites ATTENTION: without "http://" section and without trailing slash.
   - Connection to Development and Production databases data
   - Select which actions can appears on Production server (select radiobutton "Yes" or "No")
   - On section "Modules tables" select "Yes" on tables which data can be changed on Production site 
      ("No" variant selected by default).
   - On section "Variable table settings" select "Yes" for variables from "Variable" table which data can be changed on 
      Production site ("No" variant selected by default).
   - And press "Assemble build file" button.
   - After this action on Drupal "files" folder will be created directory "synchro" which will contains 
      build.xml file and folders with internal build scripts.

2. Usage Apache Ant build.xml

 - At first need install Apache Ant utility if it not installed yet:
   - Detailed information about installation Apache Ant You can find on this page:
      http://ant.apache.org/manual/install.html
 - Visit "Configuration" -> "Synchro settings" page.
   - Click "Synchro scan variables sets" for rescan variables list.
   - Look at "Synchro settings" page and make changes on settings if it needed.
   - Press "Assemble build file" button for reassemble build.xml file settings.
 - Open console and go to Your Drupal "files" folder, then to directory "synchro".
 - On this folder You can find build.xml file.
 - Available commands:

   - Run "sudo ant merge_to_dev" or just "sudo ant":
     - Move all data changes of Production site to Development site.

   - Run "sudo ant restore_dev_db"
     - Return Development Database to previous state (before run "sudo ant merge_to_dev" or just "sudo ant").

   - Run "sudo ant deploy_db"
     - Can be Run only after running "sudo ant merge_to_dev" or just "sudo ant".
        Make database deploy to Production previously merged Development database.

   - Run "sudo ant restore_prod_db"
     - Return Production Database to previous state (before run "sudo ant deploy_db").

 - "merge_to_dev" actions and troubleshooting:

 - "compare_tables_list" target errors:
   - At first script compare tables list on Production and Development servers with list of described on script tables
   - During tables comparison process can appears next errors:
     - BUILD FAILED: "This tables must be removed from script: SETTINGS/DATA SECTION: list, of, tables...",
        "This tables must be added to script tables list to "Settings" or "Data" sectionn:".
       - Script build.xml contains tables which need to be removed from tables list on sections SETTINGS or DATA.
       - If You see this message - try to reassemble build.xml file and try again.
         - Visit "Configuration" -> "Synchro settings" page.
           - Click "Synchro scan variables sets" for rescan variables list.
           - Look at "Synchro settings" page and make changes on settings if it needed.
           - Press "Assemble build file" button for reassemble build.xml file settings.
       - If reassemble build.xml file not helps You need remove/add listed tables manualy:
           - You need open build.xml file on text editor and find 
              line 50 if need remove tables from DATA section (<property name="drupal_data_tables" value="..."/>)
              line 51 if need remove tables from SETTINGS section (<property name="drupal_settings_tables" value="..."/>)
           - On value="..." section listed script tables separated by space 
              You need find needed tables on this section, remove/add it and save file.
     - BUILD FAILED: "Check manualy production tables structure. This tables lost on production: list, of, tables..."
       - In this case You have strange situation when tables from some reasons removed from Production site.
          You need to check this tables on Development site and understand why this tables removed from Production site
          and restore this tables from Development site or remove this tables from Development site.
     - "Tables: list, of, tables... Removed from Development database but presented on Production DB."
       - This is Just info message about changes which not need any actions for fixing.

 - "compare_data_tables_structure" target errors:
   - If tables structure (means fields, field types, indexes, etc.) on Development DB and Production DB is different
      BUILD will be FAILED and script show difference on diff ".patch" format.
   - Comparing processed between Production and Development databases structure.
      Means if script show patch with "+" symbol on begin of the line means this change need make on Production DB.
      If script show patch with "-" symbol on begin of the line means this change need make on Development DB.